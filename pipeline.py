#!/usr/bin/env python

import yaml

with open("pipeline.yml", 'r') as stream:
    try:
        print(yaml.load(stream))
    except yaml.YAMLError as exc:
        print(exc)